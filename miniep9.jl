
using Test
using LinearAlgebra

# Parte 1
function multiplica(a, b)
    dima = size(a)
    dimb = size(b)
    if dima[2] != dimb[1]
        return -1 
    end

    c = zeros(dima[1], dimb[2])
    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
                c[i, j] = c[i, j] + a[i, k]*b[k, j]
            end
        end
    end
    return c
end

function matrix_pot(M, p)
    pot = M
    for i in 2:p
    pot = multiplica(pot, M)
    end
    return pot
end

function test1()
    @test matrix_pot([1 2; 3 4], 1) == [1 2; 3 4]
    @test matrix_pot([1 2; 3 4], 2) == [1 2; 3 4]*[1 2; 3 4]
    println("Final dos Testes")
end

# test1()

# Parte 2
function matrix_pot_by_squaring(M, p)
    if p == 0
        return 1
    elseif p == 1
        return M
    elseif p%2 == 0
        return matrix_pot_by_squaring(multiplica(M, M), p/2)
    elseif p%2 == 1
        return matrix_pot_by_squaring(multiplica(M, M), (p -1)/2)
    end
end

function test2()
    @test matrix_pot([1 2; 3 4], 1) == [1 2; 3 4]
    @test matrix_pot([1 2; 3 4], 2) == [1 2; 3 4]*[1 2; 3 4]
    println("Final dos Testes")
end

# test2()

# Parte 3

function compare_times()
    M = Matrix(LinearAlgebra.I, 30, 30)

    @time matrix_pot(M, 10)
    @time matrix_pot_by_squaring(M, 10)
end

# compare_times()

